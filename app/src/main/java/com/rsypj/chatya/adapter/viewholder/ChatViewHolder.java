package com.rsypj.chatya.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rsypj.chatya.LocalDatabase;
import com.rsypj.chatya.R;
import com.rsypj.chatya.model.ChatModel;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatViewHolder extends RecyclerView.ViewHolder {

    private LinearLayout chatLeftWrapper;
    private LinearLayout chatRightWrapper;
    private TextView chatLeft;
    private TextView chatRight;
    private TextView usernameLeft;
    private TextView usernameRight;

    public ChatViewHolder(View view) {
        super(view);
    }

    private void initView(View view) {
        chatLeft = view.findViewById(R.id.chatBubbleLeft);
        chatRight = view.findViewById(R.id.chatBubbleRight);
        chatLeftWrapper = view.findViewById(R.id.leftWrapper);
        chatRightWrapper = view.findViewById(R.id.rightWrapper);
        usernameLeft = view.findViewById(R.id.leftUsernameText);
        usernameRight = view.findViewById(R.id.rightUsernameText);
    }

    public void setUpUI(ChatModel model) {
        if(LocalDatabase.getEmail().equals(model.getNama())) {
            chatRightWrapper.setVisibility(View.VISIBLE);
            chatRight.setText(model.getMessage());
            usernameRight.setText(model.getNama());
        }
        else {
            chatLeftWrapper.setVisibility(View.VISIBLE);
            chatLeft.setText(model.getMessage());
            usernameLeft.setText(model.getNama());
        }
    }
}
