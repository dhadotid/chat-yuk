package com.rsypj.chatya.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rsypj.chatya.R;
import com.rsypj.chatya.model.ChatListModel;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatListViewHolder extends RecyclerView.ViewHolder {

    TextView tvNama;
    TextView tvChat;
    RelativeLayout rvChat;

    public ChatListViewHolder(View view) {
        super(view);

        tvNama = view.findViewById(R.id.chatList_tvNama);
        tvChat = view.findViewById(R.id.chatList_tvChat);
        rvChat = view.findViewById(R.id.chatList);

        onChatListClicked();
    }

    public void setUpToUI(ChatListModel data){
        tvNama.setText(data.getNama());
        tvChat.setText(data.getChat());
    }

    private void onChatListClicked(){
        rvChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}
