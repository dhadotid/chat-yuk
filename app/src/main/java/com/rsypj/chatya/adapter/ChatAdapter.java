package com.rsypj.chatya.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rsypj.chatya.R;
import com.rsypj.chatya.adapter.viewholder.ChatViewHolder;
import com.rsypj.chatya.model.ChatModel;

import java.util.ArrayList;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder> {

    private Context context;
    private ArrayList<ChatModel> data;

    public ChatAdapter(Context context, ArrayList<ChatModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_chat, parent, false);
        return new ChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        holder.setUpUI(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
