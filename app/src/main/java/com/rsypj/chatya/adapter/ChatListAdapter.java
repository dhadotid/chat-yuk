package com.rsypj.chatya.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rsypj.chatya.R;
import com.rsypj.chatya.adapter.viewholder.ChatListViewHolder;
import com.rsypj.chatya.model.ChatListModel;

import java.util.ArrayList;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<ChatListModel> data = new ArrayList<>();

    public ChatListAdapter(Context context, ArrayList<ChatListModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_chat_list, null);
        return new ChatListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ChatListViewHolder){
            ChatListModel chatListModel = data.get(position);

            ((ChatListViewHolder) holder).setUpToUI(chatListModel);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
