package com.rsypj.chatya;

import android.app.Application;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class CustomApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        LocalDatabase.getSingleton(this);

    }
}
