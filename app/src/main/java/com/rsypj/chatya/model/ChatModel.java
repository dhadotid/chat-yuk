package com.rsypj.chatya.model;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatModel {

    private int id;
    private String nama;
    private String message;

    public ChatModel(int id, String nama, String message) {
        this.id = id;
        this.nama = nama;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getMessage() {
        return message;
    }
}
