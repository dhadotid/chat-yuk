package com.rsypj.chatya.model;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatListModel {

    private int id;
    private String nama;
    private String chat;

    public ChatListModel(int id, String nama, String chat) {
        this.id = id;
        this.nama = nama;
        this.chat = chat;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getChat() {
        return chat;
    }
}
