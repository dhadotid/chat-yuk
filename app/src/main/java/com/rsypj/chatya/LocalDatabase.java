package com.rsypj.chatya;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class LocalDatabase {

    private static LocalDatabase singleton;

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public static LocalDatabase getSingleton(Context context) {
        if (singleton == null) {
            singleton = new LocalDatabase(context);
        }
        return singleton;
    }

    public LocalDatabase(Context context) {
        sharedPreferences = context.getSharedPreferences("app", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static void setEmail(String email) {
        editor.putString("email", email);
        editor.commit();
    }

    public static void setToken(String token){
        editor.putString("token", token);
        editor.commit();
    }

    public static String getToken(){
        return sharedPreferences.getString("token", "");
    }

    public static String getEmail() {
        return sharedPreferences.getString("email", "");
    }
}
