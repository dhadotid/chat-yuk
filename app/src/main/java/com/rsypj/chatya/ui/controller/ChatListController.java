package com.rsypj.chatya.ui.controller;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;

import com.rsypj.chatya.adapter.ChatListAdapter;
import com.rsypj.chatya.model.ChatListModel;
import com.rsypj.chatya.ui.ChatListActivity;

import java.util.ArrayList;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatListController {

    ArrayList<ChatListModel> data = new ArrayList<>();

    ChatListActivity activity;
    ChatListAdapter adapter;

    public ChatListController(ChatListActivity activity) {
        this.activity = activity;

        setAdapter();
    }

    private void setAdapter(){
        adapter = new ChatListAdapter(activity, data);

        activity.getRcChat().setLayoutManager(new LinearLayoutManager(activity));
        activity.getRcChat().setItemAnimator(new DefaultItemAnimator());
        activity.getRcChat().setAdapter(adapter);
    }
}
