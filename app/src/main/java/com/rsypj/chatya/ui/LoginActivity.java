package com.rsypj.chatya.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.SignInButton;
import com.rsypj.chatya.R;
import com.rsypj.chatya.base.ChatBaseActivity;
import com.rsypj.chatya.ui.controller.LoginController;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class LoginActivity extends ChatBaseActivity {

    LoginController controller;
    SignInButton btnLogin;

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initiateItem() {

        btnLogin = findViewById(R.id.btnLogin);

        controller = new LoginController(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        controller.onActivityResult(requestCode, resultCode, data);
    }

    public SignInButton getBtnLogin() {
        return btnLogin;
    }
}
