package com.rsypj.chatya.ui.controller;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;

import com.rsypj.chatya.adapter.ChatAdapter;
import com.rsypj.chatya.model.ChatModel;

import java.util.ArrayList;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatController {

    ChatActivity activity;
    ChatAdapter adapter;
    ArrayList<ChatModel> data = new ArrayList<>();

    public ChatController(ChatActivity activity) {
        this.activity = activity;

        initListChatAdapter();
    }

    private void initListChatAdapter() {
        adapter = new ChatAdapter(activity, data);

        activity.getListChat().setItemAnimator(new DefaultItemAnimator());
        activity.getListChat().setLayoutManager(new LinearLayoutManager(activity));
        activity.getListChat().setAdapter(adapter);
    }
}
