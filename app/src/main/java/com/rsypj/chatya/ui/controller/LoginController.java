package com.rsypj.chatya.ui.controller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rsypj.chatya.LocalDatabase;
import com.rsypj.chatya.ui.ChatListActivity;
import com.rsypj.chatya.ui.LoginActivity;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class LoginController {

    private static int signCode = 86;
    LoginActivity activity;

    GoogleSignInOptions googleSignInOptions;
    GoogleApiClient googleApiClient;

    public LoginController(LoginActivity activity) {
        this.activity = activity;

        checkLoggedIn();
        initGoogleSignIn();
        onSignInClicked();
    }

    private void checkLoggedIn() {
        if (!LocalDatabase.getEmail().equals("")) {
            //activity.showToast("Sudah Login!");
            Intent in = new Intent(activity, ChatListActivity.class);
            activity.startActivity(in);
            activity.finish();
        }
    }

    private void initGoogleSignIn(){
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        googleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    private void onSignInClicked(){
        activity.showProgressDialog("Login", "Loading ..", false);

        activity.getBtnLogin().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInGoogle = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                activity.startActivityForResult(signInGoogle, signCode);
            }
        });
    }

    private void onLoginResult(GoogleSignInResult result){
        activity.dismisProgressDialog();

        if (result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            //activity.showToast("Berhasil Login");
            LocalDatabase.setEmail(account.getDisplayName());

            if (LocalDatabase.getToken().equals("") || !LocalDatabase.getToken().equals(FirebaseInstanceId.getInstance().getToken())){
                FirebaseInstanceId.getInstance().getToken();
            }

            Intent in = new Intent(activity, ChatListActivity.class);
            activity.startActivity(in);
            activity.finish();
        }else {
            activity.showToast("Login Error");
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == signCode){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            onLoginResult(result);
        }
    }
}
