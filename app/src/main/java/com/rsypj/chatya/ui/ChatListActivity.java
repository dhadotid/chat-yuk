package com.rsypj.chatya.ui;

import android.support.v7.widget.RecyclerView;

import com.rsypj.chatya.R;
import com.rsypj.chatya.base.ChatBaseActivity;
import com.rsypj.chatya.ui.controller.ChatListController;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatListActivity extends ChatBaseActivity {

    RecyclerView rcChat;
    ChatListController controller;

    @Override
    public int getLayoutId() {
        return R.layout.activity_chat_list;
    }

    @Override
    public void initiateItem() {
        rcChat = findViewById(R.id.activity_chat_list_recyclerView);

        controller = new ChatListController(this);
    }

    public RecyclerView getRcChat() {
        return rcChat;
    }
}
