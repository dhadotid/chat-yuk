package com.rsypj.chatya.ui.controller;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.rsypj.chatya.R;
import com.rsypj.chatya.base.ChatBaseActivity;

/**
 * Created by dhadotid on 25/02/2018.
 */

public class ChatActivity extends ChatBaseActivity {

    private RecyclerView listChat;
    private FloatingActionButton buttonSend;
    private EditText textInput;

    ChatController controller;

    @Override
    public int getLayoutId() {
        return R.layout.activity_chat;
    }

    @Override
    public void initiateItem() {
        listChat = findViewById(R.id.contentChat);
        buttonSend = findViewById(R.id.sendButton);
        textInput = findViewById(R.id.editTextChat);

        controller = new ChatController(this);
    }

    public RecyclerView getListChat() {
        return listChat;
    }

    public FloatingActionButton getButtonSend() {
        return buttonSend;
    }

    public EditText getTextInput() {
        return textInput;
    }
}
